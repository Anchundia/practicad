package com.example.cristina_a.practica4;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, FragUno.OnFragmentInteractionListener, FragDos.OnFragmentInteractionListener{
    Button botonFragUno, botonFragDos;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        botonFragUno = (Button) findViewById(R.id.btnFragUnox);
        botonFragDos = (Button) findViewById(R.id.btnFragDos);

        botonFragUno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, FragUno.class);
                startActivity(intent);
            }
        });

        botonFragDos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, FragDos.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onClick(View v) {
       switch (v.getId()){
           case R.id.btnFragUnox:
               FragUno fragmentoUno = new  FragUno();
                FragmentTransaction trasacctionUno = getSupportFragmentManager().beginTransaction();
                trasacctionUno.replace(R.id.contenedor, fragmentoUno);
                trasacctionUno.commit();
                break;
           case R.id.btnFragDos:
               FragDos fragmentoDos = new FragDos();
               FragmentTransaction trasacctionDos = getSupportFragmentManager().beginTransaction();
               trasacctionDos.replace(R.id.contenedor, fragmentoDos);
               trasacctionDos.commit();
               break;
       }
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
         menuInflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.opcionLogin:
                Dialog dialogoLogin = new  Dialog(MainActivity.this);
                dialogoLogin.setContentView(R.layout.dlg_login);
                Button botonAutentificar = (Button) dialogoLogin.findViewById(R.id.btnAutentificar);
                final EditText cajaUsuario = (EditText) dialogoLogin.findViewById(R.id.txtUser);
                final  EditText cajaClave = (EditText) dialogoLogin.findViewById(R.id .txtPassword);
                botonAutentificar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(MainActivity.this,cajaUsuario.getText().toString()+ ""+ cajaClave.getText().toString(), Toast.LENGTH_SHORT).show();
                    }
                });
                dialogoLogin.show();
                break;
            case R.id.opcionRegistrar:
                break;
        }


            return true;
    }


}
